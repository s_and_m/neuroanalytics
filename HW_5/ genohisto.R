options(stringsAsFactors=FALSE);

dped <- "/nas/longleaf/home/shm/NBIO_Assignments/HW_4/hapmap1_nomissing_AF.ped";
dmap <- "/nas/longleaf/home/shm/NBIO_Assignments/HW_4/hapmap1_nomissing_AF.map";

allSNPs <- read.table(dped);
onlySNPs <- allSNPs[,7:ncol(allSNPs)];
nSNPs <- ncol(onlySNPs)/2;
ndonor <- nrow(onlySNPs);

SNPs <- matrix(NA,nrow=ndonor,ncol=nSNPs);

for (i in 1:nSNPs){
  
  recodedSNP <- matrix(NA,nrow=ndonor,ncol=1);
  
  onesnp <- onlySNPs[,c(i*2-1,i*2)];
  
  index_NA <- which(onesnp[,1]==0 & onesnp[,2]==0);
  index_homo1 <- which(onesnp[,1]==1 & onesnp[,2]==1);
  index_hetero <- which((onesnp[,1]==1 & onesnp[,2]==2) | (onesnp[,1]==2 & onesnp[,2]==1));
  index_homo2 <- which(onesnp[,1]==2 & onesnp[,2]==2);
  
  recodedSNP[index_NA] = NA;
  recodedSNP[index_homo1] = 0;
  recodedSNP[index_hetero] = 1;
  recodedSNP[index_homo2] = 2;
  
  
  SNPs[,i] = recodedSNP;
}

#for map file
allmap <- read.table(dmap);
colnames(SNPs)=allmap$V2;
rownames(SNPs)=allSNPs[,1];

#removes NAs from SNPs
SNPs_nomissing <- colSums(SNPs,na.rm = TRUE);
num_alleles <- 2*colSums(!is.na(SNPs))

#find SNPS with allele frequency >95% and <5%
#removes people with the same SNPs despite different phenotypes
allele_freq <- SNPs_nomissing/num_alleles;

#alleles with more >95%, <5%
for (i in 1:length(allele_freq)) {
  j <- allele_freq[i];
  if (j>0.5) {
    allele_freq[i] <- 1-j;
  }
}

allele_freq <- as.data.frame(allele_freq);

# plot frequency histogram

pdf("/nas/longleaf/home/shm/NBIO_Assignments/HW_5/genohisto.pdf")

ggplot(data=allele_freq,aes(allele_freq)) +
  geom_histogram(binwidth=0.02) +
  labs(x="Minor Allele Frequency",y="Allele Frequency Count",title="Histogram of Minor Allele Frequency");

dev.off()


