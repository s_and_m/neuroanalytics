library(tidyverse);
options(stringsAsFactors=FALSE);

alc <- "/nas/longleaf/home/shm/NBIO_Assignments/HW_5/20414.gwas.imputed_v3.both_sexes.tsv";

alcdata <- read_tsv(alc);
#remove NaN values
min_pvalue <- arrange(alcdata,pval);
min_pvalue[1,]

split_variant <- str_split(alcdata$variant,":",simplify=TRUE);

split_variant <- as.data.frame(split_variant);
relevant_alcdata <- cbind(split_variant,alcdata$pval);

relevant_split_variant <- filter(relevant_alcdata,relevant_alcdata$V1=="4" & relevant_alcdata$`alcdata$pval`<0.0001);

relevant_split_variant$V2 <- as.numeric(relevant_split_variant$V2);

