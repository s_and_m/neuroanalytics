#!/bin/bash

#use external output in script
TEST=hapmap1.ped
echo "$TEST";

#for loop using seq to iterate through each line of hapmap1.ped
#wc to find # of lines to iterate through
#-l denotes file linecount
#add back quotes around variable value
LENGTHHAP=`wc -l $TEST`
echo $LENGTHHAP
LENGTH=`basename $LENGTHHAP`
echo $LENGTH

for i in $(seq 1 $LENGTH) 
do
    #outputs each line of hapmap1.ped
    head -n ${i} $TEST | tail -n 1 
    #waits 1 second between display of each line
    sleep 1;
done



#alternative??
#while IFS= read -r line; do
 # printf '%s\n' "$line";
  #sleep 1;
#done < hapmap1.ped
