#!/bin/bash

#current issue: only running once

# to make the loop turn 10 times
for i in `seq 1 10`
  do
    VAR1=${RANDOM:0:2}
    # if i is even execute counterclockwise spin
      if (( $VAR1 %2)); then
        # -n stops echo from inserting a newline character
        # -e allows echo to interpret backslash escape character '\'
        # the first \ before the second \ preserves the literal value of the following character
        # in this case, allowing for the literal \ to be printed instead of the escape character \
        echo -ne '\\';  
        # sleep command delays execution of script for a certain amount of time
        sleep 0.25;
        # \b simulates a backspace, making the text cursor move back one and 
        # overwrite the existing character, essentially deleting the previous character
        echo -ne '\b-';
        sleep 0.25;
        echo -ne '\b/'
        sleep 0.25;
        echo -ne '\b|';
        sleep 0.25;
        echo -ne '\b\\';
        sleep 0.25;
        echo -ne '\b-';
        sleep 0.25;
        echo -ne '\b';
        sleep 0.25;
        echo -ne '\b|';
        # \n is an escape sequence that inserts a newline character into a string 
        # basically calls for a new line
        # echo -ne "\b \n";
      # else if i is odd execute clockwise spin
      else
        echo -ne '/';
        sleep 0.25;
        echo -ne '\b-';
        sleep 0.25;
        echo -ne '\b\\'
        sleep 0.25;
        echo -ne '\b|';
        sleep 0.25;
        echo -ne '\b/';
        sleep 0.25;
        echo -ne '\b-';
        sleep 0.25;
        echo -ne '\b\\';
        sleep 0.25;
        echo -ne '\b|';
        # echo -ne "\b \n";
      fi
done
