#!/bin/bash

mkdir fakedata

for i in `seq 1 50`
do
    mkdir fakedata/donor${i}
    mv donor${i}_tp*.txt fakedata/donor${i}; 
done
