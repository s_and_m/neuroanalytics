#!/bin/bash

#donor for loop
for i in `seq 1 100`
do
  if [ ${i} -lt 10 ];
    then i="donor00${i}";
  elif [ ${i} -lt 100 ];
    then i="donor0${i}";
  elif [ ${i} -lt 1000 ];
    then i="donor${i}";
  fi;
  #representing the 10 time points
  for j in `seq 1 10`
  do 
    if [ ${j} -lt 10 ];
      then j="tp00${j}";
    elif [ ${j} -lt 100 ];
      then j="tp0${j}";
    fi;
    #header row in files
    #generating random numbers
    (echo "data" 
    echo $RANDOM 
    echo $RANDOM 
    echo $RANDOM 
    echo $RANDOM 
    echo $RANDOM) >> "${i}_${j}".txt
    #mv donor${i}_tp${j}.txt `printf %03d%s
  done
done

#converting to files with leading zeros

